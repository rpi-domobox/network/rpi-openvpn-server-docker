# OpenVPN Server for RPI

## DISCLAIMER
**FOR THIS USAGE THERE ARE NETWORK AND SECURITY ASPECTS TO HAVE TO KNOW ABSOLUTELTY!**  
**DO NOT OPEN THE PORTS OF YOUR ROUTEUR WITHOUT KNOWLEDGE !**

## Without Docker Compose
**Warning**  
It's recommended to use the **ovpn-data-** prefix to operate seamlessly with the reference systemd service.  
Users are encourage to replace example with a descriptive name of their choosing.  
```sh
OVPN_DATA="ovpn-data-example"
```

### Initialize the configuration files and certificates
```sh
sudo docker run -v {VOLUME_NAME}:/etc/openvpn --rm mjenz/rpi-openvpn ovpn_genconfig -u tcp://{SERVER_IP}:443
sudo docker run -v {VOLUME_NAME}:/etc/openvpn --rm -it mjenz/rpi-openvpn ovpn_initpki
```

### Start OpenVPN server process
```sh
sudo docker run -v {VOLUME_NAME}:/etc/openvpn -d -p 443:1194/tcp --cap-add=NET_ADMIN --name openvpn mjenz/rpi-openvpn
```

### Generate a client certificate
```sh
sudo docker run -v {VOLUME_NAME}:/etc/openvpn --rm -it mjenz/rpi-openvpn easyrsa build-client-full {CLIENT_NAME} nopass
```

### Retrieve the client configuration with embedded certificates
```sh
sudo docker run -v {VOLUME_NAME}:/etc/openvpn --rm mjenz/rpi-openvpn ovpn_getclient {CLIENT_NAME} > {PROFILE_FILENAME}.ovpn
```

## With Docker Compose
**Warning**  
It's recommended to use the **ovpn-data-** prefix to operate seamlessly with the reference systemd service.  
Users are encourage to replace example with a descriptive name of their choosing.  
```sh
OVPN_DATA="ovpn-data-example"
```

### Add a new service in docker-compose.yml
```sh
version: '3.7'

services:
  openvpn:
    container_name: openvpn
    image: mjenz/rpi-openvpn
    cap_add:
      - NET_ADMIN
    volumes:
      - {VOLUME_NAME}:/etc/openvpn
    ports:
      - "443:1194/tcp"
    restart: always

volumes:
  {VOLUME_NAME}:
```

### Initialize the configuration files and certificates
```sh
sudo docker-compose run --rm openvpn ovpn_genconfig -u tcp://{SERVER_IP}:443
sudo docker-compose run --rm openvpn ovpn_initpki
```

### Start OpenVPN server process
```sh
sudo docker-compose up -d openvpn
```  

### Generate a client certificate
```sh
export CLIENTNAME="your_client_name"

# with a passphrase (recommended)
sudo docker-compose run --rm openvpn easyrsa build-client-full $CLIENTNAME

# without a passphrase (not recommended)
sudo docker-compose run --rm openvpn easyrsa build-client-full $CLIENTNAME nopass
```

### Retrieve the client configuration with embedded certificates
```sh
sudo docker-compose run --rm openvpn ovpn_getclient $CLIENTNAME > $CLIENTNAME.ovpn
```

### Revoke a client certificate
```sh
# Keep the corresponding crt, key and req files.
sudo docker-compose run --rm openvpn ovpn_revokeclient $CLIENTNAME
  
# Remove the corresponding crt, key and req files.
sudo docker-compose run --rm openvpn ovpn_revokeclient $CLIENTNAME remove
```

### You can access the container logs with
```sh
sudo docker-compose logs -f
```

## Debugging Tips  
### Create an environment variable with the name DEBUG and value of 1 to enable debug output (using "docker -e").
```sh
sudo docker-compose run -e DEBUG=1 openvpn
```